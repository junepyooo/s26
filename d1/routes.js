const http = require("http");

//create a variable "port" to store the port number
const port = 4000;

//create a variable "server"that stores the ooutput of the "createServer" method
const server = http.createServer((request, response) => {
    //Accesing the 'greeting' route that returns a message of "Hello Again"
    //http://localhost:4000/greeting
    //"request" is an object that is sent via the client(browser)
    //The "url" property refers to the url or the link in the browser
    if (request.url == '/greeting') {
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('Hello Again')

    } else if (request.url == '/homepage') {
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('This is the hompage')
    } else {
        response.writeHead(404, { 'Content-Type': 'text/plain' })
        response.end('Page not found!!')
    }
})

server.listen(port);

//when server is running, console will print the message:
console.log(`Server now accessible at localhost:${port}`);


