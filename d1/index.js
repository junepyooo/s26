//use the require directive to load node js modules
//A 'module' is a software component or part of a program that contains one or more routines
let http = require("http");
//the 'http module' can transfer data using the Hyper Text Transfer Protocol
//It is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
//HTTP is a protocol that allows the fetching of resources such as HTML documents.
//Client (browser) and server (nodeJS) communicate by exchanging individual messages.
//The messages sent by the client, usually a web browser, are called 'requests'
//The messages sent by the server as an answer are called "responses"


//Using this module's createServer() method, we can create an HTTP server that listens to requests on a specified address/Url/port and gives responses back to the client

//A port is a virtual point where network connections start and end.
//Each port is associated with a specific process or service
//http://localhost//4000
//The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to our server


http.createServer(function (request, response) {

    //we use the writeHead() method to:
    //Set status code for the response - a 200 means ok
    //Set the content-type of the response as a plain text message
    //Value of content type can be image/jpeg or application/json
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    //send  the response

    response.end("Hello World")
}).listen(4000)

console.log('Server running at localhost:4000');