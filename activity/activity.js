// - What directive is used by Node.js in loading the modules it needs?
//answer: require directive

// - What Node.js module contains a method for server creation?
//answer: http module


// - What is the method of the http object responsible for creating a server using Node.js?
//answer: createServer() method


// - What method of the response object allows us to set status codes and content types?
//answer: writeHead() method


// - Where will console.log() output its contents when run in Node.js?
//answer: terminal


// - What property of the request object contains the address's endpoint?
//answer: .listen